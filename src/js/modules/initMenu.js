function initMenu() {
  console.log("initMenu");

  const heightHeader = document.querySelector(".header").offsetHeight;
  const heightHeaderMobile = document.querySelector(
    ".header__wrapper-mobile"
  ).offsetHeight;

  if (window.matchMedia("(min-width: 1023px)").matches) {
    let page = document.querySelector(".ident-menu-js");
    page.style.paddingTop = heightHeader + "px";
  }

  if (window.matchMedia("(max-width: 1023px)").matches) {
    let pageMobile = document.querySelector(".ident-menu-js");
    pageMobile.style.paddingTop = heightHeaderMobile + "px";
  }

  //Плавная прокрутка. Якоря
  //вместо a[href^="#"] можно добавлять класс cсылки или индификатор
  //   $(document).ready(function(){
  //     $('a[href^="#"]').bind("click", function(e){
  //         var anchor = $(this);
  //         if (window.matchMedia('(max-width: 928px)').matches) {
  //           $('html, body').stop().animate({
  //               scrollTop: $(anchor.attr('href')).offset().top
  //           }, 800);
  //         } else {
  //           $('html, body').stop().animate({
  //             scrollTop: $(anchor.attr('href')).offset().top - heightHeader
  //           }, 800);
  //         }
  //         e.preventDefault();
  //         document.body.classList.remove('body-scroll-hidden');
  //     });
  //     return false;
  // });

  //Плавная прокрутка. Якоря
  //вместо a[href^="#"] можно добавлять класс cсылки или индификатор
  try {
    const anchors = document.querySelectorAll('a[href*="#"]');

    for (let anchor of anchors) {
      anchor.addEventListener("click", function (e) {
        const blockID = anchor.getAttribute("href").substr(1);
        if (blockID.length >= 2) {
          e.preventDefault();
          try {
            document.getElementById(blockID).scrollIntoView({
              behavior: "smooth",
              block: "start",
            });
          } catch {
            console.log("Ошибка initSmoothScrolling (scrollIntoView)");
          }
        }
      });
    }

    // Mobile menu
    if (window.matchMedia("(max-width: 1023px)").matches) {
      const mainCatalog = document.querySelector(".section-catalog__wrapper");
      const catMenu = document.querySelectorAll(".list-menu-catalog");
      const firstUl = document.querySelectorAll(".first-ul");
      const firstLi = document.querySelectorAll(".first-li");
      const secondUl = document.querySelectorAll(".second-ul");
      const secondLi = document.querySelectorAll(".second-li");

      // console.log(firstUl);

      const newArray = splice(0, 0, firstUl);

      console.log(`newArray: ${newArray}`);

      const btnCatalog = document.querySelector(
        ".mobile-header__button-catalog"
      );

      btnCatalog.addEventListener("click", () => {
        document.body.classList.toggle("mobile-menu-active");
      });
      function blabla() {
        catMenu.forEach((item, index) => {
          console.log(`idemId ${item}`, `indexId: ${firstUl.index}`);
          item.value.addEventListener("click", (itemId, indexId) => {
            console.log(`idemId ${itemId}`, `indexId: ${secondUl[indexId]}`);
          });
        });
      }

      // console.log("defef");
      // mainCatalog.addEventListener("mousedown", () => {});

      // document.addEventListener("mousedown", (event) => {
      //   if (event.target.closest(".list-menu-catalog")) {
      //     const element = event.target
      //       .closest(".list-menu-catalog")
      //       .querySelector(".Catalog__list-product-second-level");
      //     $(element).slideToggle(300);
      //   }
      // });
    }
  } catch {
    console.log("Ошибка initSmoothScrolling");
  }

  //фиксированная шапка при скроле
  // $("header").removeClass("default");
  // $(window).scroll(function(){
  //   if ($(this).scrollTop() > 40){
  //     $("header").addClass("default").fadeIn("fast");
  //   } else{
  //     $("header").removeClass("default").fadeIn("fast");
  //   };
  // });

  //активное меню
  // $(document).ready(function(){
  //   $(".container div:first a").addClass("active"); //выделяет активную страничку
  //     $(".container div").click(function(event){
  //       $(".container div a").removeClass("active");//удаляет все активные элементы
  //       $(this).find("a").addClass("active");
  //     });
  // });
}
