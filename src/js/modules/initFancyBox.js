function initFancyBox() {
	console.log("initFancyBox");
	$.extend(true, $.fancybox.defaults, {
		lang: 'ru',
	  i18n: {
		 ru: {
			CLOSE: 'Закрыть',
			NEXT: 'Вперед',
			PREV: 'Назад',
			ERROR: 'Запрашиваемый контент не может быть загружен. <br/> Пожалуйста, попробуйте позже.',
			PLAY_START: 'Начать демонстрацию',
			PLAY_STOP: 'Приостановить демонстрацию',
			FULL_SCREEN: 'На полный экран',
			THUMBS: 'Миниатюры',
			DOWNLOAD: 'Скачать',
			SHARE: 'Поделиться',
			ZOOM: 'Увеличить',
		 },
	  },
	});
  
	$('[data-fancybox-window]').fancybox({
	  buttons: [],
	  touch: false,
	  modal: false,
	  hideOnOverlayClick: true,
	  enableEscapeButton: true,
	  clickOutside: true,
	  afterClose: function() {
		document.body.classList.remove('body-scroll-hidden');
	  },
	});
	
	$('[data-fancybox-window]').click(function() {
		document.body.classList.add('body-scroll-hidden');
	});

	$('[data-fancybox="images-preview"]').fancybox({
		thumbs : {
			loop: true,
			touch: false
		}
	 });
  }