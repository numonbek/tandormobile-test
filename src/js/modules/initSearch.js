function initSearch() {
  console.log("initSearch");
  try {
    const searchIcon = document.querySelectorAll(".Search__block-icon");
    const SearchBlock = document.querySelectorAll(".Search__block");
    const SearchForm = document.querySelectorAll(".Search__form");
    const blockButtons = document.querySelector(
      ".header__block-buttons-js"
    ).offsetWidth;
    const blockButtonsMedia = document.querySelector(
      ".header__adaptive-block-buttons"
    ).offsetWidth;

    let searchIconIndex;
    SearchBlock.forEach(function (itemBlock) {
      searchIcon.forEach(function (itemIcon) {
        SearchForm.forEach(function (itemForm) {
          function searchMedia(mediaQuery) {
            if (
              window.matchMedia("(max-width: 955px)").matches &&
              window.matchMedia("(mix-width: " + mediaQuery + "px)")
            ) {
              itemBlock.style.width = blockButtonsMedia - 10 + "px";
              console.log(blockButtonsMedia);
            } else if (
              window.matchMedia("(max-width: " + mediaQuery + "px)").matches
            ) {
              itemBlock.style.width =
                document.documentElement.clientWidth - 30 + "px";
            } else {
              console.log(blockButtons);

              itemBlock.style.width = blockButtons - 100 + "px";
            }
          }

          itemIcon.addEventListener("click", function (event) {
            searchIconIndex = itemIcon.getAttribute("data-search-index");
            itemIcon.classList.add("Search__block-icon--open");

            if (
              searchIconIndex === itemBlock.getAttribute("data-search-index")
            ) {
              itemBlock.classList.add("Search__block--open");
              searchMedia(550);
            }

            if (
              searchIconIndex === itemForm.getAttribute("data-search-index")
            ) {
              itemForm.classList.add("Search__form--open");
            }
          });

          document.addEventListener("mousedown", function (event) {
            let searchStatus = "";

            searchStatus = itemBlock.contains(event.target);
            if (
              searchStatus === false &&
              searchIconIndex === itemBlock.getAttribute("data-search-index")
            ) {
              itemBlock.classList.remove("Search__block--open");
              itemBlock.removeAttribute("style");

              itemForm.classList.remove("Search__form--open");

              itemIcon.classList.remove("Search__block-icon--open");
            }
          });
        });
      });
    });
  } catch {
    console.log("Ошибка initSearch");
  }
}
