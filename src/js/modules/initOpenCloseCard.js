function initOpenCloseCard() {
  console.log("initOpenCloseCard");
  try {
    const button = document.querySelectorAll(".Card-product__have-icon-js");
    const contentArr = document.querySelectorAll(
      ".Card-product__block-have-info-js"
    );
    let content, cardProduct;

    button.forEach(function (itemButton) {
      itemButton.addEventListener("click", (event) => {
        cardProduct = event.target.closest(".Card-product");
        content = cardProduct.querySelector(
          ".Card-product__block-have-info-js"
        );
        content.classList.toggle("Card-product__block-have-info--active");
        if (!content.hasAttribute("data-card-info")) {
          setTimeout(() => {
            content.setAttribute("data-card-info", "");
          }, 0);
        }
        contentArr.forEach(function (itemContentArr) {
          if (itemContentArr.hasAttribute("data-card-info")) {
            itemContentArr.classList.remove(
              "Card-product__block-have-info--active"
            );
            itemContentArr.removeAttribute("data-card-info");
          }
        });
      });
    });

    document.addEventListener("click", (event) => {
      if (!event.target.closest(".Card-product__have-icon-js")) {
        contentArr.forEach(function (itemContentArr) {
          itemContentArr.classList.remove(
            "Card-product__block-have-info--active"
          );
          itemContentArr.removeAttribute("data-card-info");
        });
      }
    });
  } catch {
    console.log("Ошибка initOpenCloseCard");
  }

  try {
    const button = document.querySelectorAll(
      ".Card-product-aflat__have-icon-js"
    );
    const contentArr = document.querySelectorAll(
      ".Card-product-aflat__block-have-info-js"
    );
    let content, cardProduct;

    button.forEach(function (itemButton) {
      itemButton.addEventListener("click", (event) => {
        cardProduct = event.target.closest(".Card-product-aflat");
        content = cardProduct.querySelector(
          ".Card-product-aflat__block-have-info-js"
        );
        content.classList.toggle("Card-product-aflat__block-have-info--active");
        if (!content.hasAttribute("data-card-info")) {
          setTimeout(() => {
            content.setAttribute("data-card-info", "");
          }, 0);
        }
        contentArr.forEach(function (itemContentArr) {
          if (itemContentArr.hasAttribute("data-card-info")) {
            itemContentArr.classList.remove(
              "Card-product-aflat__block-have-info--active"
            );
            itemContentArr.removeAttribute("data-card-info");
            console.log(itemContentArr);
          }
        });
      });
    });

    document.addEventListener("click", (event) => {
      if (!event.target.closest(".Card-product-aflat__have-icon-js")) {
        contentArr.forEach(function (itemContentArr) {
          itemContentArr.classList.remove(
            "Card-product-aflat__block-have-info--active"
          );
          content.removeAttribute("data-card-info");
        });
      }
    });
  } catch {
    console.log("Ошибка initOpenCloseCardAflat");
  }
}
