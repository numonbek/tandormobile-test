function initCounter() {
  console.log("initCounter");
  try {
    document.addEventListener("click", (e) => {
      const input = e.target.closest(".Counter");
      if (!input) return;

      const inpControl = input.querySelector("input");
      const btnDecrement = e.target.closest("[data-decrement]");
      const btnIncrement = e.target.closest("[data-increment]");

      let dataValue, dataMin, dataMax, dataStep;

      dataValue = Number(inpControl.getAttribute("value"));
      dataMin = Number(inpControl.getAttribute("min"));
      dataMax = Number(inpControl.getAttribute("max"));
      dataStep = Number(inpControl.getAttribute("step"));

      function funDec() {
        if (btnDecrement) {
          if (parseInt(inpControl.value) > dataMin) {
            // btnDecrement.removeAttribute("disabled");
            inpControl.value = +inpControl.value - dataStep;
          }
          // else {
          //   btnDecrement.setAttribute("disabled", "");
          // }
        }
      }
      function funInc() {
        if (btnIncrement) {
          if (parseInt(inpControl.value) < dataMax) {
            // btnIncrement.removeAttribute("disabled");
            inpControl.value = +inpControl.value + dataStep;
          }
          // else {
          //   btnIncrement.setAttribute("disabled", "");
          // }
        }
      }

      funDec();
      funInc();

      inpControl.oninput = function () {
        if (inpControl.value < dataMin - 1) {
          inpControl.value = dataMin;
        }

        if (inpControl.value > dataMax) {
          inpControl.value = dataMax;
        }
        funDec();
        funInc();
      };
    });
  } catch {
    console.log("Ошибка initCounter");
  }
}
